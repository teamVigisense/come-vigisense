package com.come.vigisense.android.core;

import com.come.vigisense.android.model.BodyScale;
import com.come.vigisense.android.model.BpMonitor;
import com.come.vigisense.android.model.Fibrillation;
import com.come.vigisense.android.model.Medication;
import com.come.vigisense.android.model.MioFuseBattery;
import com.come.vigisense.android.model.MioFuseDailyActivity;
import com.come.vigisense.android.model.MioFuseHeartRate;
import com.come.vigisense.android.model.MioFuseHeartRateDay;
import com.come.vigisense.android.model.MioFuseHeartRateDelta;
import com.come.vigisense.android.model.MioFuseHeartRateDeltaDay;
import com.come.vigisense.android.model.MioFuseHeartRateDeltaHour;
import com.come.vigisense.android.model.MioFuseHeartRateHour;
import com.come.vigisense.android.model.MyBean;
import com.come.vigisense.android.model.Oximeter;
import com.come.vigisense.android.model.Questionnaire;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.come.vigisense.api.androidApi.AndroidApi;

import java.util.ArrayList;
import java.util.HashMap;

public class PlugYouTables {
    static ArrayList<MyBean> tables = new ArrayList<>();
    private static boolean init;
    private static HashMap<MyBean, MySyncAdapter.SyncCmd> tablesSync = new HashMap<>();


    public static Iterable<? extends MyBean> getTables() {
        init();
        return tables;
    }

    private static void init() {
        if (init)
            return;
        init = true;
        add(new BodyScale(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return  myApiService.insertBodyScale(row.toApiObj(new com.come.vigisense.api.androidApi.model.BodyScale()));
            }
        });
        add(new BpMonitor(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertBpMonitor(row.toApiObj(new com.come.vigisense.api.androidApi.model.BpMonitor()));
            }
        });
//        add(new Pedometer(), new MySyncAdapter.SyncCmd() {
//            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
//                return myApiService.insertPedometer(row.toApiObj(new com.come.vigisense.api.androidApi.model.Pedometer()));
//            }
//        });

        add(new MioFuseDailyActivity(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertMioFuseDailyActivity(row.toApiObj(new com.come.vigisense.api.androidApi.model.MioFuseDailyActivity()));
            }
        });

        add(new MioFuseHeartRateHour(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertHRHour(row.toApiObj(new com.come.vigisense.api.androidApi.model.MioFuseHeartRateHour()));
            }
        });

        add(new MioFuseHeartRateDay(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertHRDay(row.toApiObj(new com.come.vigisense.api.androidApi.model.MioFuseHeartRateDay()));
            }
        });
        add(new MioFuseHeartRateDeltaHour(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertHRDeltaHour(row.toApiObj(new com.come.vigisense.api.androidApi.model.MioFuseHeartRateDeltaHour()));
            }
        });

        add(new MioFuseHeartRateDeltaDay(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertHRDeltaDay(row.toApiObj(new com.come.vigisense.api.androidApi.model.MioFuseHeartRateDeltaDay()));
            }
        });
        add(new Medication(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertMedication(row.toApiObj(new com.come.vigisense.api.androidApi.model.Medication()));
            }
        });

        // never sync
//        add(new MioFuseHeartRate(), new MySyncAdapter.SyncCmd() {
//            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
//                return myApiService.insertMioFuseHeartRate(row.toApiObj(new com.come.vigisense.api.androidApi.model.MioFuseHeartRate()));
//            }
//        });

        add(new MioFuseBattery(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertMioFuseBattery(row.toApiObj(new com.come.vigisense.api.androidApi.model.MioFuseBattery()));
            }
        });
        add(new Oximeter(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertOximeter(row.toApiObj(new com.come.vigisense.api.androidApi.model.Oximeter()));
            }
        });

        add(new Questionnaire(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertQuestionnaire(row.toApiObj(new com.come.vigisense.api.androidApi.model.Questionnaire()));
            }
        });
        add(new Fibrillation(), new MySyncAdapter.SyncCmd() {
            public AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception {
                return myApiService.insertFibrillation(row.toApiObj(new com.come.vigisense.api.androidApi.model.Fibrillation()));
            }
        });

        tables.add(new MioFuseHeartRate());
        tables.add(new MioFuseHeartRateDelta());
    }

    private static void add(MyBean table, MySyncAdapter.SyncCmd syncCmd) {
        tables.add(table);
        tablesSync.put(table,syncCmd);
    }

    public static HashMap<MyBean, MySyncAdapter.SyncCmd> getTablesSync() {
        init();
        return tablesSync;
    }

}
