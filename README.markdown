# Come: Vigisense Components Integration #

## Architecture ##
![Architecture.png](https://docs.google.com/drawings/d/e/2PACX-1vSGTA4AOMczmQBXHeH0yHyULlq87VBG5LAhXiLN9yP5mH6se3Z4LFGo7soVTXXZkNvgD0RwLugNxyn8/pub?w=415&amp;h=222)

## Features ##

 * Supported bluetooth devices :
    * Aoem U80E blood pressure monitor
    * Yunmai body scale
    * Mio Fuse. [User guide](http://help.mioglobal.com/hc/en-us/categories/200229234-Mio-FUSE-)
 * Extra fields :
    * heart rate every minute. Server synchronisation every hour. 
    * heart rate aggregation daily and hourly

* Minimum android version : 5.1
* OAuth 2.0 is still not enabled, but could be esaily beacause it is a built-in feature

## Server component ##
**Subscribe to get new data**

```
#!java
import com.come.vigisense.server.api.Message;
//POST https://vigisense-api.appspot.com/_ah/api/partnerApi/v1/registerListener/http%3A%2F%2Fyour-domain.com%2Fyour-path
URL urlToQueryAsHttpPost = new java.net.URL(Message.SERVER_API_URL + "/registerListener/"+ URLEncoder.encode("http://your-domain.com/your-path", "UTF-8"));
```

* Example source code: [com.come.vigisense.server.example.RegisterListener](https://bitbucket.org/teamVigisense/come-vigisense/src/9c1183321cb6a8c814449ad7e0c246e3ea92c1e7/serverApi/src/main/java/com/come/vigisense/server/example/RegisterListener.java?at=master&fileviewer=file-view-default)

* Live test: [http://vigisense-api.appspot.com/RegisterListener](http://vigisense-api.appspot.com/RegisterListener)

**Handle published data (JSON)**


```
#!java
import com.come.vigisense.server.model.BodyScale;
import com.google.gson.*; //or other JSON library
BodyScale BodyScale = new Gson().fromJson(URLDecoder.decode(postedString, "UTF-8"), BodyScale.class);
```


* Field name references: [com.come.vigisense.server.api.Message](https://bitbucket.org/teamVigisense/come-vigisense/src/9c1183321cb6a8c814449ad7e0c246e3ea92c1e7/serverApi/src/main/java/com/come/vigisense/server/api/Message.java?at=master&fileviewer=file-view-default)

* Example source code: [com.come.vigisense.server.example.HandlePush](https://bitbucket.org/teamVigisense/come-vigisense/src/9c1183321cb6a8c814449ad7e0c246e3ea92c1e7/serverApi/src/main/java/com/come/vigisense/server/example/HandlePush.java?at=master&fileviewer=file-view-default)

**Pull data**

```
#!java
import com.come.vigisense.server.api.Message;
import com.come.vigisense.server.model.BodyScale;
import com.google.gson.*; //or other JSON library
//GET https://vigisense-api.appspot.com/_ah/api/partnerApi/v1/collectionresponse_bodyscale/demoUserId?limit=10
URL urlToQueryAsHttpGet = new java.net.URL(Message.SERVER_API_URL + "/collectionresponse_bodyscale/"+someUserId);
...
List<BodyScale> bodyScales = new Gson().fromJson(new JsonParser().parse(URLDecoder.decode(responseOfHttpGetMethod, "UTF-8")).getAsJsonObject().get("items").getAsJsonArray(),  new TypeToken<List<BodyScale>>(){}.getType());

```
* Explore the pull api:  https://apis-explorer.appspot.com/apis-explorer/?base=https://vigisense-api.appspot.com/_ah/api#p/partnerApi/v1/

* Example source code: [com.come.vigisense.server.example.PullData](https://bitbucket.org/teamVigisense/come-vigisense/src/9c1183321cb6a8c814449ad7e0c246e3ea92c1e7/serverApi/src/main/java/com/come/vigisense/server/example/PullData.java?at=master&fileviewer=file-view-default)

* Live test: [http://vigisense-api.appspot.com/PullData](http://vigisense-api.appspot.com/PullData)

**Access to server backend**

Access to the Google Cloud Platform project ([logs](https://console.cloud.google.com/logs?project=vigisense-api), [database](https://console.cloud.google.com/datastore?project=vigisense-api), ...): [https://console.cloud.google.com/?project=vigisense-api](https://console.cloud.google.com/?project=vigisense-api) 

 * login: vigisense.partner@gmail.com
 * password: vigisense

**Create demo data**

You can create data sample generated randomly using the url [http://vigisense-api.appspot.com/CreateDemoData](http://vigisense-api.appspot.com/CreateDemoData)

For example to create sample data for last 5 days for the user token "demo123" : [http://vigisense-api.appspot.com/CreateDemoData?userId=demo123&days=5](http://vigisense-api.appspot.com/CreateDemoData?userId=demo123&days=5)

## Android component ##

* Install manually from the [apk](http://vigisense.com/vigimage/come-vigisense.apk)