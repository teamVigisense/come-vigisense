
package com.come.vigisense.server.core;

import com.come.vigisense.server.api.Message;
import com.come.vigisense.server.model.Fibrillation;
import com.come.vigisense.server.model.MioFuseDailyActivity;
import com.come.vigisense.server.model.MioFuseHeartRateDay;
import com.come.vigisense.server.model.MioFuseHeartRateDeltaDay;
import com.come.vigisense.server.model.MioFuseHeartRateDeltaHour;
import com.come.vigisense.server.model.MioFuseHeartRateHour;
import com.come.vigisense.server.model.ServerListener;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.RetryOptions;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.repackaged.com.google.gson.Gson;
import com.google.appengine.repackaged.com.google.gson.JsonElement;
import com.come.vigisense.server.model.BodyScale;
import com.come.vigisense.server.model.BpMonitor;
import com.come.vigisense.server.model.Medication;
import com.come.vigisense.server.model.MioFuseBattery;
import com.come.vigisense.server.model.Oximeter;
import com.come.vigisense.server.model.Questionnaire;

import java.util.logging.Level;
import java.util.logging.Logger;

@Api(
        name = "androidApi",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.come.com",
                ownerName = "api.vigisense.come.com",
                packagePath = ""
        )
)

public class AndroidEndpoint {

    private static final Logger log;

    static {
        log = Logger.getLogger(AndroidEndpoint.class.getName());
        log.setLevel(Level.ALL);
    }

    private static Queue queue;

    @ApiMethod()
    public BodyScale insertBodyScale(BodyScale row) {
        log.info("insert: " + row);
        push(row);
        return new BodyScaleEndpoint().insert(row);

    }

    @ApiMethod()
    public BpMonitor insertBpMonitor(BpMonitor row) {
        log.info("insert: " + row);
        row = new BpMonitorEndpoint().insert(row);
        push(row);
        return row;
    }

    @ApiMethod()
    public MioFuseDailyActivity insertMioFuseDailyActivity(MioFuseDailyActivity row) {
        log.info("insert: " + row);
        push(row);
        return new MioFuseDailyActivityEndpoint().insert(row);
    }

    @ApiMethod()
    public void insertMioFuseHeartRates( @Named("userId") String userId, @Named("timesHeartRatesJsonString") String timesHeartRatesJsonString) {
//        log.info("insert, size: " + timesHeartRates.size());
//        push(row);
        new MioFuseHeartRateEndpoint().insertAll(userId,timesHeartRatesJsonString);
    }

    @ApiMethod()
    public MioFuseHeartRateHour insertHRHour(MioFuseHeartRateHour row) {
        log.info("insert: " + row);
        push(row);
        return new MioFuseHeartRateHourEndpoint().insert(row);
    }

    @ApiMethod()
    public Oximeter insertOximeter(Oximeter row) {
        log.info("insert: " + row);
        push(row);
        return new OximeterEndpoint().insert(row);
    }

    @ApiMethod()
    public MioFuseHeartRateDay insertHRDay(MioFuseHeartRateDay row) {
        log.info("insert: " + row);
        push(row);
        return new MioFuseHeartRateDayEndpoint().insert(row);
    }

    @ApiMethod()
    public MioFuseHeartRateDeltaDay insertHRDeltaDay(MioFuseHeartRateDeltaDay row) {
        log.info("insert: " + row);
        push(row);
        return new MioFuseHeartRateDeltaDayEndpoint().insert(row);
    }

    @ApiMethod()
    public MioFuseHeartRateDeltaHour insertHRDeltaHour(MioFuseHeartRateDeltaHour row) {
        log.info("insert: " + row);
        push(row);
        return new MioFuseHeartRateDeltaHourEndpoint().insert(row);
    }

    @ApiMethod()
    public MioFuseBattery insertMioFuseBattery(MioFuseBattery row) {
        log.info("insert: " + row);
        push(row);
        return new MioFuseBatteryEndpoint().insert(row);
    }


    @ApiMethod()
    public Questionnaire insertQuestionnaire(Questionnaire row) {
        log.info("insert: " + row);
        push(row);
        return new QuestionnaireEndpoint().insert(row);
    }

    @ApiMethod()
    public Fibrillation insertFibrillation(Fibrillation row) {
        log.info("insert: " + row);
        push(row);
        return new FibrillationEndpoint().insert(row);
    }
    @ApiMethod()
    public Medication insertMedication(Medication row) {
        log.info("insert: " + row);
        push(row);
        return new MedicationEndpoint().insert(row);
    }

    public static String push(Object row) {
        CollectionResponse<ServerListener> servers = new ServerListenerEndpoint().list(null, 50);
        JsonElement jsonElement = new Gson().toJsonTree(row);
        jsonElement.getAsJsonObject().addProperty(Message.TABLE_NAME, row.getClass().getSimpleName());
        String jsonString = jsonElement.toString();
        for (ServerListener server: servers.getItems()) {
            try {
//                PushTask.push(jsonString,server.getUrl());
                addTask(jsonString, server);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonString;
    }

    private static void addTask(String jsonString, ServerListener server) {
        if (queue==null)
            queue = QueueFactory.getDefaultQueue();
        queue.add(TaskOptions.Builder.withUrl("/"+PushTask.class.getSimpleName())
                .method(TaskOptions.Method.POST)
                .param(PushTask.ROW, jsonString)
                .param(PushTask.URL, server.getUrl())
                .retryOptions(RetryOptions.Builder.withTaskRetryLimit(3))
        );
    }



}
