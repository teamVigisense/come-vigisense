/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * This code was generated by https://github.com/google/apis-client-generator/
 * (build: 2017-11-07 19:12:12 UTC)
 * on 2017-12-14 at 16:16:15 UTC 
 * Modify at your own risk.
 */

package com.come.vigisense.api.medicationApi.model;

/**
 * Model definition for Medication.
 *
 * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
 * transmitted over HTTP when working with the medicationApi. For a detailed explanation see:
 * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">https://developers.google.com/api-client-library/java/google-http-java-client/json</a>
 * </p>
 *
 * @author Google, Inc.
 */
@SuppressWarnings("javadoc")
public final class Medication extends com.google.api.client.json.GenericJson {

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key("_id") @com.google.api.client.json.JsonString
  private java.lang.Long id;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.String json;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key @com.google.api.client.json.JsonString
  private java.lang.Long timestampMs;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.String userId;

  /**
   * @return value or {@code null} for none
   */
  public java.lang.Long getId() {
    return id;
  }

  /**
   * @param id id or {@code null} for none
   */
  public Medication setId(java.lang.Long id) {
    this.id = id;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getJson() {
    return json;
  }

  /**
   * @param json json or {@code null} for none
   */
  public Medication setJson(java.lang.String json) {
    this.json = json;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.Long getTimestampMs() {
    return timestampMs;
  }

  /**
   * @param timestampMs timestampMs or {@code null} for none
   */
  public Medication setTimestampMs(java.lang.Long timestampMs) {
    this.timestampMs = timestampMs;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getUserId() {
    return userId;
  }

  /**
   * @param userId userId or {@code null} for none
   */
  public Medication setUserId(java.lang.String userId) {
    this.userId = userId;
    return this;
  }

  @Override
  public Medication set(String fieldName, Object value) {
    return (Medication) super.set(fieldName, value);
  }

  @Override
  public Medication clone() {
    return (Medication) super.clone();
  }

}
